import turtle

radius = eval(input("Enter the radius : "))
tr = turtle.Turtle()

tr.speed(10)
# set thickness for each ring
tr.pensize(5)

tr.color("blue")
tr.penup()
tr.goto(radius*(-2) - 20, 20-radius)
tr.pendown()
tr.circle(radius)

tr.color("black")
tr.penup()
tr.goto(0, 20 - radius)
tr.pendown()
tr.circle(radius)

tr.color("red")
tr.penup()
tr.goto(radius * 2 + 20, 20-radius)
tr.pendown()
tr.circle(radius)

tr.color("yellow")
tr.penup()
tr.goto(radius * (-1) - 10, radius*(-2) + 15)
tr.pendown()
tr.circle(radius)

tr.color("green")
tr.penup()
tr.goto(radius + 10, radius*(-2) + 15)
tr.pendown()
tr.circle(radius)

tr.screen.exitonclick()
