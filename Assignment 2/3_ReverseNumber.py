# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

num = int(input("Enter 4 digit number : "))
reverse = 0

while num > 0:
    reverse = reverse * 10 + num % 10
    num //= 10

print("Reverse of number is : ", reverse)
