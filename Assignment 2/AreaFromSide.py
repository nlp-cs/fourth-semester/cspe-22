# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

from math import tan


def calc_area(side):
    return (5 * (side ** 2)) / (4 * tan(3.14 / 5))


def main():
    s = eval(input("Enter side of pentagon : "))
    print("Area of given pentagon is : ", calc_area(s))


if __name__ == "__main__":
    main()

