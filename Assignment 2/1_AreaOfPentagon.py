# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

from math import sin
from AreaFromSide import calc_area

r = eval(input("Enter distance from center to vertex of pentagon : "))

side = 2 * r * sin(3.14/5)
print("Area of pentagon is :", calc_area(side))
