# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

number = int(input("Enter number : "))

if number%30 == 0:
    print("Yes, number is divisible by both 5 and 6")
elif number % 5 == 0:
    print("Number is divisible by 5, but not 6")
elif number % 6 == 0:
    print("Number is divisible by 6, but not 5")
else:
    print("Number is neither divisible by 5 nor 6")
