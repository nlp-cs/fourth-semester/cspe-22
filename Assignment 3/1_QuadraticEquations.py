# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

from math import sqrt

a = eval(input("Enter a : "))
b = eval(input("Enter b : "))
c = eval(input("Enter c : "))

d = sqrt((b*b) - (4*a*c))

if d == 0:
    print((- b) / (2*a))

if d > 0:
    print("Roots are : ", (0 - d - b) / (2*a), " ", (d - b) / (2*a))

if d < 0:
    print("No real roots exist")
