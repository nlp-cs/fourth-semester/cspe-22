# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

days = {
    "1": 31,
    "3": 31,
    "4": 30,
    "5": 31,
    "6": 30,
    "7": 31,
    "8": 31,
    "9": 30,
    "10": 31,
    "11": 30,
    "12": 31,
}

month = input("Enter month : ")
year = eval(input("Enter year : "))

if month == "2":
    if year % 4 == 0:
        print("Days in given month are : 29")
    else:
        print("Days in given month are : 28")
else:
    print("Days in given month are : ", days[month])
