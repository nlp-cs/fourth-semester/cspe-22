# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

a = eval(input("Enter a : "))
b = eval(input("Enter b : "))
c = eval(input("Enter c : "))
d = eval(input("Enter d : "))
e = eval(input("Enter e : "))
f = eval(input("Enter f : "))

if a*d-b*c == 0:
    print("No solution exist")
    exit()

print("x = ", (e*d-b*f) / (a*d - b*c))
print("y = ", (a*f-e*c) / (a*d - b*c))
