# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

days = {
    0: "Sunday",
    1: "Monday",
    2: "Tuesday",
    3: "Wednesday",
    4: "Thursday",
    5: "Friday",
    6: "Saturday"
}

curr_day = input("Enter Day : ")

day_number = list(days.keys())[list(days.values()).index(curr_day)]

day_number = (day_number + int(input("Enter number of days : "))) % 7

print("Day is : ", days[day_number])
