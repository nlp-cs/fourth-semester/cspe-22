# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

sides = [eval(input("Enter side 1 : ")), eval(input("Enter side 2 : ")), eval(input("Enter side 3 : "))]

sides.sort()

if sides[2] > sides[0] + sides[1]:
    print("Sorry, invalid input")
else:
    print("Perimeter of triangle is : ", sides[0]+sides[1]+sides[2])
