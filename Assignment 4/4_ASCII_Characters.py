# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

chars = 0
init = ord("!")
final = ord("~")

for i in range(init, final + 1):
    print(chr(i), end=" ")
    chars += 1
    if chars % 10 == 0:
        print()
