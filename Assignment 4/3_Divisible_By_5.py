# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

numbers = 0

for i in range(100, 1001):
    if i % 5 == 0 and i % 6 == 0:
        print(i, end=" ")
        numbers += 1
        if numbers % 10 == 0:
            print()
