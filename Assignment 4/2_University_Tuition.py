# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

tuition = 10000

for i in range(0, 10):
    tuition *= 1.05

print("Tuition after 10 years = ", tuition)

tut_sum = 0
for i in range(0, 4):
    tut_sum += tuition
    tuition *= 1.05

print("Cost of 4 years of tuition after 10 years is : ", tut_sum)
