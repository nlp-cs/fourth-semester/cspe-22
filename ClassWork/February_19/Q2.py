# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

my_list = [1, 2, 3, 4, 5, 6, 7, 8, 2, 3, 4, 5, 6, 7, 9]

freq = dict()

for i in my_list:
    if i in freq:
        freq[i] += 1
    else:
        freq[i] = 1

print("Frequencies of elements are : ")
for i in sorted(freq.keys()):
    print(i, "\t", freq[i])
