# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

phone_directory = dict()

choice = 0

while choice != 7:
    print("=========================================================================================")
    print("1. Display name and phone numbers of all the friends")
    print("2. Enter Key value pair and display updated dictionary")
    print("3. Delete particular friend from dictionary")
    print("4. Change phone number of existing friend")
    print("5. Check if friend is present in dictionary or not")
    print("6. Display Dictionary in sorted order of names")
    print("7. Quit")

    choice = int(input("\nEnter choice :"))
    print("\n")

    match choice:
        case 1:
            print("Name and phone numbers of friends are ")
            for i in phone_directory:
                print(i, " ", phone_directory[i])
            print("\n")
        case 2:
            friend = input("Enter name : ")
            phone_directory[friend] = int(input("Enter phone number : "))
            print("Updated Name and phone numbers of friends are ")
            for i in phone_directory:
                print(i, " ", phone_directory[i])
            print("\n")
        case 3:
            friend = input("Enter friend to delete : ")
            if friend in phone_directory:
                print("Removed ", friend)
            else:
                print("There is no such record in directory.")
            print("\n")
        case 4:
            friend = input("Enter friend name : ")
            phone_directory[friend] = int(input("Enter phone number : "))
            print("Phone number updated successfully!")
        case 5:
            friend = input("Enter friend to delete : ")
            if friend in phone_directory:
                print("Yes, ", friend, " is present in dictionary")
            else:
                print("No, friend is not present in dictionary.")
            print("\n")
        case 6:
            for i in sorted(phone_directory.keys()):
                print(i, " ", phone_directory[i])
        case 7:
            print("Thank you!")
