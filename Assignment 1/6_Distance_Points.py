# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

import math

print("Enter x coordinate of first point : ", end=" ")
x1 = eval(input())

print("Enter y coordinate of first point : ", end=" ")
y1 = eval(input())

print("Enter x coordinate of second point : ", end=" ")
x2 = eval(input())

print("Enter y coordinate of second point : ", end=" ")
y2 = eval(input())

print("Distance between these two points are : ", math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2))
