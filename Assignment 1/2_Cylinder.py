# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

import math

print("Enter radius : ", end=" ")
radius = eval(input())
print("Enter height : ", end=" ")
height = eval(input())
area = radius * radius * math.pi
print("Area is : ", area)

print("Volume is : ", area * height)
