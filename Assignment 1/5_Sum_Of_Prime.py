# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

def check_prime(number):
    i = 2
    while i * i <= number:
        if (number % i) == 0 and number != i:
            return False
        i += 1
    return True


sum_primes = 0
print("Enter number : ", end=" ")
n = int(input())
for i in range(2, n + 1):
    if check_prime(i):
        sum_primes += i

print("Sum of all primes upto given number is : ", sum_primes)
