# Made by Naman Garg
# Contact : <12112061@nitkkr.ac.in>

print("Enter the weight of water : ", end=" ")
weight = eval(input())

print("Enter the initial temperature of water : ", end=" ")
init_temp = eval(input())

print("Enter the final temperature of water : ", end=" ")
final_temp = eval(input())

print("Energy needed (in joules) : ", weight * (final_temp - init_temp) * 4184)
